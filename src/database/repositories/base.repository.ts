import { NotFoundException } from "@nestjs/common";
import { DeepPartial, Repository } from "typeorm";
import IRepository from "../interfaces/repository.interface";

abstract class BaseRepository<T> implements IRepository<T> {
  protected constructor(private readonly entity: Repository<T>) {}

  create(data: T): Promise<T> {
    return this.entity.save(data as DeepPartial<T>);
  }

  findOneById(id: number, relations?: string[]): Promise<T> {
    return this.entity.findOne(id, { relations });
  }

  findByCondition(filterCondition: any, relations?: string[]): Promise<T[]> {
    return this.entity.find({ where: filterCondition, relations });
  }

  findAll(relations?: string[]): Promise<T[]> {
    return this.entity.find({
      relations,
      loadRelationIds: true,
    });
  }

  async update(id: number, newData: any): Promise<T> {
    const updated = await this.entity.update(id, newData);

    if (!updated.affected) {
      throw new NotFoundException();
    }

    return this.findOneById(id);
  }

  async remove(id: string): Promise<boolean> {
    const deleted = await this.entity.softDelete(id);

    if (!deleted.affected) {
      throw new NotFoundException();
    }

    return true;
  }
}

export default BaseRepository;
