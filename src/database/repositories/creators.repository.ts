import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import BaseRepository from "./base.repository";
import { Creator } from "../../creators/entities/creator.entity";

@Injectable()
class CreatorsRepository extends BaseRepository<Creator> {
  constructor(@InjectRepository(Creator) private readonly creatorRepository: Repository<Creator>) {
    super(creatorRepository);
  }
}

export default CreatorsRepository;
