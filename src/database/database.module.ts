import { Global, Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Creator } from "../creators/entities/creator.entity";
import CreatorsRepository from "./repositories/creators.repository";

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: "postgres",
          host: configService.get("POSTGRES_HOST"),
          port: configService.get("POSTGRES_PORT"),
          database: configService.get("POSTGRES_DB"),
          username: configService.get("POSTGRES_USER"),
          password: configService.get("POSTGRES_PASSWORD"),
          entities: [__dirname + "/../**/*.entity{.ts,.js}"],
          synchronize: true,
        };
      },
    }),
    TypeOrmModule.forFeature([Creator]),
  ],
  providers: [CreatorsRepository],
  exports: [CreatorsRepository],
})
export class DatabaseModule {}
