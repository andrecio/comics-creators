interface IRepository<T> {
  create(data: T): Promise<T>;

  findOneById(id: number, relations?: any): Promise<T>;

  findByCondition(filterCondition: any, relations?: any): Promise<T[]>;

  findAll(relations?: any): Promise<T[]>;

  update(id: number, newData: any): Promise<T>;

  remove(id: string): Promise<boolean>;
}

export default IRepository;
