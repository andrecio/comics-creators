import { type ConfigModuleOptions } from "@nestjs/config";
import * as Joi from "joi";
import { join } from "path";

export const appConfig: ConfigModuleOptions = {
  isGlobal: true,
  validationSchema: Joi.object({
    NODE_ENV: Joi.string()
      .valid("development", "test", "production", "evaluate")
      .default("development"),
    PORT: Joi.number().default(3000),
    POSTGRES_HOST: Joi.string().required(),
    POSTGRES_PORT: Joi.number().required(),
    POSTGRES_USER: Joi.string().required(),
    POSTGRES_PASSWORD: Joi.string().required(),
    POSTGRES_DB: Joi.string().required(),
  }),
  envFilePath: join("docker", `${process.env.NODE_ENV}.env`),
  validationOptions: {
    abortEarly: true,
  },
};
